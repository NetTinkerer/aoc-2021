#!/usr/bin/python3

import sys
import numpy as np
from pprint import pprint

COORDINATES = ['x1', 'y1', 'x2', 'y2']
DANGER_VALUE = 2
class ThermoMeasurement:
    def __init__(self,measurement_dict):
        for key in measurement_dict:
            measurement_dict[key] = int(measurement_dict[key])
        self.measurement_dict = measurement_dict
        self.straight = False
        if self.measurement_dict['x1'] == self.measurement_dict['x2'] or self.measurement_dict['y1'] == self.measurement_dict['y2']: self.straight = True

class ThermoScanner:
    def __init__(self):
        self.measurements=[]
        self.limits = {'x':0,'y':0}

    def add_measurement(self,measurement):
        start, delim, end = measurement.split()
        measurement_dict = {}
        
        measurement_dict['x1'],measurement_dict['y1'] = start.split(',')
        measurement_dict['x2'],measurement_dict['y2'] = end.split(',')
        self.measurements.append(ThermoMeasurement(measurement_dict))
        for value in ['x1', 'x2']:
            if measurement_dict[value] > self.limits['x']: self.limits['x'] =  measurement_dict[value]
        for value in ['y1', 'y2']:
            if measurement_dict[value] > self.limits['y']: self.limits['y'] =  measurement_dict[value]

    def show_measurements(self):
        for measurement in self.measurements:
            print([measurement.measurement_dict[val] for val in COORDINATES ])
            for val in measurement.measurement_dict:
                print(f"{val}:{measurement.measurement_dict[val]}")
    
    def init_map(self):
        self.thermo_map = np.full((self.limits['x']+1,self.limits['y']+1),0)
    
    def add_measurements_to_map(self,straight=False, diag=False):
        for measurement in self.measurements:
            x_dir = 1 if measurement.measurement_dict['x1'] <= measurement.measurement_dict['x2'] else -1                    
            y_dir = 1 if measurement.measurement_dict['y1'] <= measurement.measurement_dict['y2'] else -1
            
            if measurement.straight and straight:
                x_start = measurement.measurement_dict['x1']
                x_end = measurement.measurement_dict['x2']
                y_start = measurement.measurement_dict['y1']
                y_end = measurement.measurement_dict['y2']
                if x_start==x_end:
                    for i in range(0,abs(y_start-y_end)+1):
                        self.thermo_map[y_start,x_start] += 1
                        y_start += y_dir
                if y_start==y_end:
                    for i in range(0,abs(x_start-x_end)+1):
                        self.thermo_map[y_start,x_start] +=1
                        x_start += x_dir
            if not measurement.straight and diag:
                x_start = measurement.measurement_dict['x1']
                x_end = measurement.measurement_dict['x2']
                y_start = measurement.measurement_dict['y1']
                y_end = measurement.measurement_dict['y2']
                
                x_dir = 1 if measurement.measurement_dict['x1'] <= measurement.measurement_dict['x2'] else -1
                y_dir = 1 if measurement.measurement_dict['y1'] <= measurement.measurement_dict['y2'] else -1
  
                for i in range(0,abs(x_start-x_end)+1):
                    self.thermo_map[y_start,x_start] +=1
                    x_start += x_dir
                    y_start += y_dir

    def get_danger(self,danger_value):
        return np.where(self.thermo_map >= danger_value)
                
    def show_map(self):
        pprint(self.thermo_map)

            
def get_input_as_list(file_name):
    output = []
    with open(file_name) as f:
        for line in f:
            output.append(line.rstrip())
    return output

def run_program():
    input_data = get_input_as_list(sys.argv[1])
    Thermo = ThermoScanner()
    for measurement in input_data:
        Thermo.add_measurement(measurement)
#    Thermo.show_measurements()
    Thermo.init_map()
    Thermo.add_measurements_to_map(straight=True, diag=False)
#    Thermo.show_map()
    danger = Thermo.get_danger(DANGER_VALUE)
    print(f"part1: {len(danger[0])}")
    Thermo.add_measurements_to_map(straight=False, diag=True)
    danger = Thermo.get_danger(DANGER_VALUE)
    print(f"part2: {len(danger[0])}")
if __name__ == '__main__':
    run_program()