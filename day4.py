#!/usr/bin/python3

import sys
import numpy as np
from pprint import pprint
class Bingo:

    def __init__(self,card_data):
        self.card = np.array(card_data)
        self.matches = np.array([[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]])
        self.card_won = False
        self.round = 0
    
    def check_number(self,number):
        #return False if already won don't claim twice
        if self.card_won: return False
        for i in range(0,5):
            for j in range(0,5):
                if self.card[i][j] == number: self.matches[i][j] = 1
        for k in range(0,5):
            if sum(self.matches[k,:]) == 5 or sum(self.matches[:,k]) == 5:
                self.card_won = True
                return True
        return False

    def get_score(self):
        total = 0
        for i in range(0,5):
            for j in range(0,5):
                if self.matches[i][j] == 0: total += int(self.card[i][j])
        return total
                  
def get_input_as_list(file_name):
    output = []
    with open(file_name) as f:
        for line in f:
            output.append(line.rstrip())
    return output


def run_program():
    input_data = get_input_as_list(sys.argv[1])
    bingo_cards=[]
    for line in input_data:
        if ',' in line:
            bingo_numbers = line
        elif line == "":
            card_data=[]
            row_index=0
        else:
            card_data.append(line.split())
            row_index += 1
            if row_index == 5:
                bingo_cards.append(Bingo(card_data))
    print("All data read")
    winning_cards_total = 0
    for game_round, number in enumerate(bingo_numbers.split(',')):
        for card_no, card in enumerate(bingo_cards):
            
            if card.check_number(number):
                winning_cards_total += 1
                if winning_cards_total == 1:
                    print("1: {} * {} = {}".format(card.get_score(),number,card.get_score()*int(number)))
                if winning_cards_total == 100:
                    print("2: {} * {} = {}".format(card.get_score(),number,card.get_score()*int(number)))
                    exit()
                
                
            

if __name__ == '__main__':
    run_program()