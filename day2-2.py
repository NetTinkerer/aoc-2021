#!/usr/bin/python3

import sys

def get_input_as_list(file_name):
    output = []
    with open(file_name) as f:
        for line in f:
            output.append(line)
    return output

def run_program():
    input_data = get_input_as_list(sys.argv[1])
    #print(input_data)
    position = {'depth':0, 'horizontal':0, 'aim':0}
    for movement in input_data:
        direction, value = movement.split(' ')
        if direction == 'forward':
            position['horizontal'] += int(value)
            position['depth'] += int(value)*position['aim']
        elif direction == 'up':
            position['aim'] -= int(value)
        elif direction == 'down':
            position['aim'] += int(value)
        else:
            print("unknown command")
            exit(1)
    print("Depth: {}, Horizontal {}, Product {}".format(position['depth'],position['horizontal'],position['depth']*position['horizontal']))

    
if __name__ == '__main__':
    run_program()